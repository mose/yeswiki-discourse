Discourse <-> YesWiki
=========================

Ceci est un repo temporaire pour garder traces de ce qu'on doit mettre au propre

Discourse comme provider d'auth pour yeswiki
---------------------------------------------

- copier le code de ce repo dans `custom/` (en attendant que ca soit mis au propre dans une extension)
- dans discourse admin > settings > login
  - activer `enable discourse connect provider`
  - dans `discourse connect provider secrets` mettre le nom de domaine du yeswiki et une clé secrete arbitraire
- dans `wakka.config.php` ajouter:
  ```
  'discourse_connect_url' => 'https://<url-discourse>/session/sso_provider',
  'discourse_connect_secret' => '0123456789',
  ```


Discourse comme systeme de commentaire pour les pages bazar
-------------------------------------------------------

voir: https://meta.discourse.org/t/embed-discourse-comments-on-another-website-via-javascript/31963


- dans discourse ajouter un host dans `/admin/customize/embedding`
- ajouter un champs bazar custom avec dedans:
```
<div id='discourse-comments'></div>
<script type="text/javascript">
  window.DiscourseEmbed = {
    discourseUrl: 'https://discourse.testynh.distrilab.fr/',
    discourseEmbedUrl: document.location
  };
  (function() {
    var d = document.createElement('script');
    d.type = 'text/javascript';
    d.async = true;
    d.src = window.DiscourseEmbed.discourseUrl + 'javascripts/embed.js';
    (document.getElementsByTagName('head')[0] || 
     document.getElementsByTagName('body')[0]).appendChild(d);
  })();
</script>
```
(si dans customhtml, il faut tout mettre sur une seule ligne)

---

mrflos, mose & 12b